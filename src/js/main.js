(function($){

  // nonce
  var $body = $('body');
  var $window = $( window );

  function init() {

    preventHashNav();

    initDrag();

    initMobileHotspot();

    initHoverHotspot();
  }

  /**
   * Use hover detection to identify touch screens.
   * Yeah...spotty at best.
   */
   function deviceHasHover() {
    return !! ( window.matchMedia( '(any-hover: none)' ) );
  }

  /**
   * Initialize the dragon
   */
  function initDrag() {

    var $mask = $body.find( '#hero__mask' );
    var $panes = $mask.find( '.pane' );
    var $active = $panes.filter( '.active' );
    var $swipe = $mask.find( '.swipe-bar' );
    // prevent another swipe before the first on ends.
    var inProgress = false; // ends up being same as running...
    var lockDelay = 500; // in ms

    // so we can hide all of them on a swipe
    var $allHotspots = $mask.find( '.tag' );
    var $modals = $mask.find( '.modal' );

    var dragon = initDragon();

    function initDragon() {
      return {
        t: null,
        dt: null,
        x: null,
        dx: null,
        y: null,
        dy: null,
        
        start: {
          x: null,
          y: null,
          t: null
        },
        end: {
          x: null,
          y: null,
          t: null
        }
      }
    }

    // may need to add mousedown & mouseup
    $mask.on( 'mousedown.drag mouseup.drag touchstart.drag touchend.drag', dragListener );

    // prevent dumb stuff from happening while dragging
    $mask.on( 'touchchange.drag mousemove.drag', changeHandler );

    function changeHandler( e ) {
      e.preventDefault();
    }

    /**
     * Wrapper so we can catch and conditionally process drag events
     * depending on screen size and other concerns.
     * @param {event} e 
     */
    function dragListener( e ) {
      if ( ! processDragEvents() ) {
        return;
      }

      if ( 'touchend' === e.type || 'mouseup' === e.type ) {
        dragEnd( e );
      }
      else if ( 'touchstart' === e.type || 'mousedown' === e.type ) {
        dragStart( e );
      }
    }

    function dragEnd( e ) {      
      if ( dragon.start.x || 0 === dragon.start.x ) {}
        dragon.end.t = e.originalEvent.timeStamp;
        dragon.end.x = normalizeEvent( e ).clientX;
        dragon.end.y = normalizeEvent( e ).clientY;

        if ( isSwipe( dragon.start, dragon.end ) ) {
          $swipe.hide();
          var direction = getSwipeDirection( dragon.start, dragon.end );
          hideModal();
          if ( 'left' === direction ) {
            dragLeft();
          }
          else if ( 'right' === direction ) {
            dragRight();
          }

        }

    }

    /**
     * Moves panes to the right
     * @returns 
     */
     function dragLeft() {
      var activeIndex = $panes.index( $active );

      if ( $panes.length - 1 === activeIndex ){
        return;
      }

      var $next = $panes.eq( activeIndex + 1 );
      $next.addClass( 'active from-right' );
      $active.addClass( 'to-left' );


      $active.on( 'animationend.dragleft', function(e){
        $active.removeClass( 'active running to-left' );
        $active.off( 'animationend.dragleft' );
        $next.removeClass( 'running from-right' ); 
        $active = $next;
        window.setTimeout( lockDelay, unlock );
      });

      if ( 0 === activeIndex ) {
        $mask.toggleClass( 'left middle' );
      }
      if ( 1 === activeIndex ) {
        $mask.toggleClass( 'middle right' )
      }
      $next.add( $active ).addClass( 'running' );

      dragon = initDragon();

    }

    /**
     * Move things to the left
     * @returns 
     */
    function dragRight() {
      var activeIndex = $panes.index( $active );

      if ( 0 === activeIndex ){
        return;
      }
      var $next = $panes.eq( activeIndex - 1 );

      $next.addClass( 'active from-left' );
      $active.addClass( 'to-right' );

      $active.on( 'animationend.dragright', function(e){
        $active.removeClass( 'active running to-right' );
        $active.off( 'animationend.dragright' );
        $next.removeClass( 'running from-left' ); 
        $active = $next;
        window.setTimeout( lockDelay, unlock );
      });

      if ( 1 === activeIndex ) {
        $mask.toggleClass( 'left middle' );
      }
      if ( $panes.length - 1 === activeIndex ) {
        $mask.toggleClass( 'middle right' )
      }

      $next.add( $active ).addClass( 'running' );

      dragon = initDragon();
    }

    function dragStart( e ) {
      inProgress = true;
      dragon.start.t = e.originalEvent.timeStamp;
      dragon.start.x = normalizeEvent( e ).clientX;
      dragon.start.y = normalizeEvent( e ).clientY;
    }

    function getSwipeDirection( start, end ) {
      var length = end.x - start.x;
      var direction = false;
      if ( length > 0 ) {
        direction = 'right';
      }
      else if ( length < 0 ) {
        direction = 'left';
      }
      return direction;
    }

    function hideModal() {
      var $hs = $allHotspots.filter( '[aria-expanded="true"]' );
      if ( 0 < $hs.length ) {
        $modals.filter( '#' + $hs.attr( 'aria-controls' ) ).hide();
        $hs.attr( 'aria-expanded', 'false' );
        $hs.blur();
      }
    }

    // we'll say a swipe has to have a minimum distance
    function isSwipe( start, end ) {
      var threshhold = .1;
      var length = Math.abs( end.x - start.x );
      var enough = $window.outerWidth() * threshhold;
      return length >= enough;
    }

    /**
     * Normalize touch and mouse events.
     * @param {event} e 
     * @returns 
     */
    function normalizeEvent(e){
      return e.changedTouches ? e.changedTouches[0] : e;
    }

    function unlock() {
      inProgress = false;
    }

  } // dragInit();

  
  function initHoverHotspot() {
    if ( ! deviceHasHover() ) {
      return;
    }

    var $wrap = $body.find( '#desktop-hotspots' );
    var $hotspots = $wrap.find( '.tag' );
    var $modals = $wrap.find( '.modal' );

    $wrap.on( 'click', wrapClick );

    $hotspots.each(function(i){
      var $this = $hotspots.eq(i);
      var $modal = $wrap.find( '#' + $this.attr( 'aria-controls' ) );
      var $links = $modal.find( 'a' );
      var placement = $modal.hasClass( 'right' ) ? 'right' : 'left';
      var hideDelay = 1000; // in ms
      var timeoutId;

      // $this mouseenter displays the $modal
      $this.on('mouseenter', mouseEnter );

      // $this mouseleave starts a timeout to hide $modal
      $this.on('mouseleave', mouseLeave );

      // $this Enter keydown shows or hides $modal
      $this.on('keydown', tagKeydown );
      
      // taps...don't register...
      $this.on( 'touch tap', tagTap );

      /**
       * Clicks register, but are still subject to mouseleave 
       * unless we block mouseleve events if the hotspot has been 
       * clicked.
       */
      $this.on( 'click', tagClick );
      
      // $modal mouseenter
      $modal.on( 'mouseenter', mouseEnter );
      // $modal mouseleave restarts the timeout to hide the modal
      $modal.on( 'mouseleave', mouseLeave );
      $modal.on( 'keydown.hotspot', modalKeydown );

      function hide( andFocus ) {
        if ( 'undefined' === typeof andFocus ) {
          andFocus = false;
        }
        $modal.hide();
        $this.attr( 'aria-expanded', 'false' );
        $this.removeClass( 'active' );
        $body.off( 'keydown.hotspot' );
        timeoutId = null;
        $this.data('clicked', false);
        if ( true === andFocus ) {
          $this.focus();
        }
        else {
          document.activeElement.blur();
        }
      }

      function show() {
        var modalPos = getModalPosition();
        $modal.css({
          left: modalPos.left,
          top: modalPos.top
        });
        $this.addClass( 'active' );
        $modal.show();

        $this.attr( 'aria-expanded', true );

        $body.on( 'keydown.hotspot', bodyKeydown );

      }

      function bodyKeydown(e) {
        if ( 'Escape' !== e.key ) {
          return;
        }
        hide();
        $this.focus();
      }

      function getModalPosition() {
        var hsPos = $this.position();
        var modalPos = hsPos;
        // scaling factor for faking vws
        var scale = $window.outerWidth() > 1440 ? 1 : 1 / 1440;
 
        // fake VWs
        var x = 10 * scale; // ( 10 / 1440 );
        var leftVW = hsPos.left * scale; // ( hsPos.left / 1440 );
        if ( 'right' === placement ) {
          modalPos.left = ( x + leftVW ) * 1440 + $this.outerWidth();
        }
        else {
          modalPos.left = (leftVW - x) * 1440 - $modal.outerWidth();
        }
        // top should always be the same, right?
        modalPos.top = (modalPos.top + ($this.outerHeight()/2)) - ( $modal.outerHeight() / 2 );
        return modalPos;
      }

      /**
       * Need to trap tabbing while the modal is open.
       * @param {event} e 
       * @returns 
       */
      function modalKeydown( e ) {
        if ( 'Tab' !== e.key ) {
          return;
        }
        e.preventDefault();
        var index = $links.index( document.activeElement );
        if ( e.shiftKey ) {
          if ( 0 === index ) {
            index = $links.length;
          }
          index--;
        }
        else {
          index = ( index + 1 ) % $links.length;
        }
        $links[index].focus();
      }

      function mouseEnter( e ) {
        if ( timeoutId ) {
          window.clearTimeout( timeoutId );
        }
        if ( 'false' === $this.attr( 'aria-expanded' ) ) {
          hideAll();          
        }
        show();
      }

      function mouseLeave( e ) {
        // only set a timeout if click/tap wasn't used to open the modal!
        if ( false == $this.data('clicked') ) {
          timeoutId = window.setTimeout( hide, hideDelay );
        }
      }

      function tagClick( e ) {
        if ( isSyntheticClick( e.originalEvent ) ) {
          return;
        }
        e.preventDefault();
        // prevent registering click on the wrapper
        e.stopImmediatePropagation();
        // if the thing is visible and has been clicked, hide it
        if ( 'true' === $this.attr( 'aria-expanded' ) && $this.data('clicked') ){
          hide();
        }
        // otherwise, make it persistent
        else {
          $this.data('clicked', true);
          show();
          $links[0].focus();
        }

      }

      function tagKeydown(e){
        if ( !isKeyEnterOrSpace( e.key ) ) {         
          return;
        }
        e.preventDefault();
        if ( 'true' === $this.attr('aria-expanded') ) {
          hide();
          $this.focus();
        }
        else {
          hideAll();
          show();
          $links[0].focus();
        }
      }

      /**
       * This doesn't ever seem to fire in an emulator.
       * @param {*} e 
       */
      function tagTap( e ) {
        console.group( 'tagTap' );
        e.preventDefault();
        console.log( 'tapped: ', $this );
        console.log( 'tapEvent: ', e );
        console.groupEnd();
      }

      $this.data({
        pooper: {
          hide: hide,
          show: show  
        }
      })

      // console.groupEnd();
    }); // $hotspots.each()

    /**
     * close all modals except the one related to the 
     * hotspot tag
     */
    function hideAll( $not ){
      var $them = $hotspots.not( $not );
      var $this;
      $them.each(function(i){
        $this = $them.eq(i);
        $this.data('pooper').hide();
      })

    }

    function wrapClick( e ) {
      hideAll();
    }
    
  } // hoverHotspotInit()

  function initMobileHotspot(){
    var $hero = $body.find('#hero__mask');
    var $hotspots = $hero.find( '.tag' );
    var $modals = $hero.find( '.modal' );
    var $visible = $hotspots.filter('[aria-expanded="true"]');

    $hotspots.each(function(i, item){
      var $this = $hotspots.eq( i );
      var $modal;
      if ( $this.attr( 'aria-controls' ) ) {
        $modal = $modals.filter( '#' + $this.attr( 'aria-controls' ) );
        /**
         * Store the links in the modal so we trap tabbing when 
         * the modal is opened with keydown.
         */ 
        $modal.data( 'links', $modal.find('a') );

        $this.data( 'modal', $modal );
      }
    });

    $hotspots.on( 'click.mobile-hotspot tap.mobile-hotspot', hotspotClickTapHandler );

    $hotspots.on( 'keydown.mobile-hotspot', hotspotKeydown );

    function bodyKeydown( e ) {
      if ( 'Escape' !== e.key ) {
        return;
      }
      hideModal( $hotspots.filter( '[aria-expanded="true"]' ), true );
    }

    function hideModal( $hs, andFocus ){      
      if ( 1 > $hs.length ) {
        return;
      }
      if ( 'undefined' === typeof andFocus ) {
        andFocus = false;
      }
      $hs.attr( 'aria-expanded', 'false' );
      $hs.data( 'modal' ).hide();
      $body.off( 'keydown.hotspot' );
      $hero.off( 'click.mobile-hotspot', maskClick );
      $hs.data('modal').off( 'keydown.mobile-hotspot', modalKeydown );      
      $visible = $hotspots.filter( '[aria-expanded="true"]' );
      if ( true === andFocus ) {
        $hs.focus();
      }
    }

    function hotspotClickTapHandler( e ) {
      // console.group( 'clickTapHandler' );
      e.preventDefault();
    
      // prevent bubbling up to $mask
      e.stopImmediatePropagation();

      if ( isSyntheticClick( e ) ) {
        // console.log( 'synthetic click caused by pressing Enter' );
        // console.groupEnd();
        return;
      }

      var $hs = $hotspots.filter( this );
      // visible seems unnecessary...
      $visible = $modals.filter( ':visible' );

      if ( 'true' == $hs.attr( 'aria-expanded' ) ) {
        hideModal( $hs );
        // console.groupEnd();
        return;
      }

      hideModal( $hotspots.filter( '[aria-expanded="true"]' ) );

      showModal( $hs );

      // do I need $visible?
      $visible = $modals.filter( ':visible' );
      // console.groupEnd();
    }

    /**
     * Handle pressing Enter while focused on a hotspot.
     * @param {event} e 
     * @returns 
     */
    function hotspotKeydown( e ) {
      if ( ! isKeyEnterOrSpace(e.key) ) {
        return;
      }
      e.preventDefault();

      var $hs = $hotspots.filter( this );

      if ( 'true' === $hs.attr( 'aria-expanded' ) ) {
        hideModal( $hs );
      }
      else {
        showModal( $hs );
        /**
         * now focus on the first link in the modaland add modal 
         * keydown handler
         */
        $hs.data( 'modal' ).data( 'links' )[0].focus();
        $hs.data('modal').on( 'keydown.mobile-hotspot', modalKeydown );

      }

    }

    /**
     * Clicking directly on the mask should dismiss any 
     * open modals.
     * @param {event} e 
     */
    function maskClick( e ) {
      hideModal( $hotspots.filter( '[aria-expanded="true"]' ) );
    }

    /**
     * Trap tabbing when modal is open per WCAG something.
     * @param {event} e 
     */
    function modalKeydown( e ) {
      if ( 'Tab' !== e.key ) {
        return;
      }
      // prevent default tabbing while modal open
      e.preventDefault();

      var $modal = $modals.filter( this );

      var index = $modal.data( 'links' ).index( document.activeElement );
      if ( e.shiftKey ) {
        if ( 0 === index ) {
          index = $modal.data( 'links' ).length;
        }
        index--;
      }
      else {
        index = (index + 1) % $modal.data( 'links' ).length;
      }
      $modal.data( 'links' )[index].focus();

      // console.groupEnd();
    }


    function showModal( $hs ){
      $hs.attr( 'aria-expanded', 'true' );
      $hs.data( 'modal' ).show();
      $body.on( 'keydown.hotspot', bodyKeydown );
      $hero.on( 'click.mobile-hotspot', maskClick );
    }

  } // mobileHotspotInit()

  /**
   * Returns true if the key pressed was Enter or Spacebar
   * @param {*} key 
   * @returns boolean
   */
   function isKeyEnterOrSpace( key ) {
    return ( 'Enter' === key || ' ' === key || 'Spacebar' === key );
  }

  /**
   * Detects synthetic click events caused by pressing Enter
   * @param {event} event 
   * @returns boolean
   */
  function isSyntheticClick( event ) {
    return !!( 'click' === event.type && 0 === event.detail );
  }


  /**
   * Prevent navigating to url#
   */
  function preventHashNav() {
    $body.on( 'click', 'a[href="#"]', function(e){
      e.preventDefault();
      return;
    });
  }

  /**
   * This is essentially a window size check.
   * If screen < 768px wide, return true.
   * @returns boolean
   */
   function processDragEvents() {
    return !!( 768 > $window.outerWidth() );
  }


  // document.ready
  $(function(){

    init(); 

  });
})(jQuery);