const { watch } = require('gulp');
const gulp = require( 'gulp' );
const cleanCSS = require( 'gulp-clean-css' );
const concat = require( 'gulp-concat' );
const image = require( 'gulp-image' );
const rename = require( 'gulp-rename' );
const sass = require( 'gulp-sass' );
const uglify = require( 'gulp-uglify' );

sass.compiler = require( 'node-sass' );

function scss() {
  return gulp.src( './src/scss/style.scss' )
    .pipe( sass().on('error', sass.logError) )
    .pipe( gulp.dest( './src/css/' ) );  
}

function cssMin() {
  return gulp.src( './src/css/style.css' )
    .pipe( cleanCSS() )
    .pipe( rename({ extname: '.min.css' }) )
    .pipe( gulp.dest( './www/css/' ) );
}

function concatJS() {
  return gulp.src([
    './node_modules/jquery/dist/jquery.js',
    './src/js/main.js'
  ])
    .pipe( concat( 'bundle.js' ) )
    .pipe( gulp.dest( './src/js/' ) );
}

function jsMin() {
  return gulp.src( './src/js/bundle.js' )
    .pipe( uglify() )
    .pipe( rename({ extname: '.min.js' }) )
    .pipe( gulp.dest( './www/js/' ) );
}

exports.default = function() {
  gulp.watch( './src/scss/**/*.scss', 
    { ignoreInitial: false },
    gulp.series( scss, cssMin )
  );
  gulp.watch( './src/js/main.js', 
    { ignoreInitial: false },
    gulp.series( concatJS, jsMin )
  );
}

exports.imageMin = function(){
  return gulp.src( [
    './src/img/*.jpg',
    './src/img/*.png'
  ])
    .pipe( image() )
    .pipe( gulp.dest( './www/img/' ) );
}